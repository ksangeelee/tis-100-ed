# TIS-100 Puzzle Editor

A Vue app to edit TIS-100 puzzles. Includes the starlight Lua interpreter
(actually a Lua -> ES6 transpiler) so that test code can be run to generate
expected results.

## License

Copyright (C) 2020 Kevin Sangeelee

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## License

Starlight: MIT, https://github.com/paulcuth/starlight/
Vue: MIT, https://github.com/vuejs/vue

