/**
 * Gets the snippet that declares those puzzle description lines
 * that are not empty.
 */
function getDescription(descriptions) {
  var snippet = 'return { ';
  descriptions.forEach(function(d) {
    var text = d.text.trim();
    if(text.length > 0)
      snippet += '"' + text.toUpperCase() + '", ';
  });

  return snippet + " }";
}

/**
 * Gets the Lua variable declaration snippet for the input and output
 * data arrays (e.g. ina = {})
 */
function getStreamsVars(inputs, outputs) {
  var snippet = '';
  inputs
    .concat(outputs).forEach( function(obj, index) {
      if(obj.active == true)
        snippet += '  ' + streamVarFrom(obj.name) + ' = {}\n';
    });
  return snippet;
}

/**
 * Gets the Lua snippet that returns the array of stream definitions
 * (e.g. return { {STREAM_INPUT, "ina", 0, [...]}, ... })
 */
function getStreamsReturn(inputs, outputs) {
  var snippet = 'return {\n';
  inputs.forEach( function(i, index) {
    if(i.active == true)
      snippet += ' { STREAM_INPUT, "' + i.name + '", ' + index +
        ', ' + streamVarFrom(i.name) + ' },\n';
  });

  outputs.forEach( function(o, index) {
    if(o.active == true)
      snippet += ' { STREAM_OUTPUT, "' + o.name + '", ' + index +
        ', ' + streamVarFrom(o.name) + ' },\n';
  });
  snippet += '}\n';
  return snippet;
}

function getLuaSource(name, descriptions, inputs, outputs, bodyLua) {
  var str1 = 'function get_streams()\n' + getStreamsVars(inputs, outputs);
  var str2 = '\n' + bodyLua;
  var str3 = '\n' + getStreamsReturn(inputs, outputs) + '\nend\n';
  return str1 + str2 + str3;
}
