Vue.component('tis-node', {

  props: ['idx', 'type_idx'],
  template: '<div class="node" v-on:click="toggle">{{node.label}}</div>',
  computed: {
    node: function() {
      if(this.type_idx == 0)
        return {ordinal: 0, type: 'compute', label: 'COMPUTE'};
      else if(this.type_idx == 1)
        return {ordinal: 1, type: 'memory', label: 'MEMORY'};
      else
        return {ordinal: 2, type: 'damaged', label: 'DAMAGED'};

    }
  },
  methods: {
    toggle: function () {
      var current = this.idx;
      this.$emit('nodechanged', current);
    }
  }
});
